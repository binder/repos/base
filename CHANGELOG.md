# Changelog
Relevant changes &ndash; "Added", "Changed" and "Removed" &ndash; between versions of this project are documented here.

## [0.2.0] - 2020-06-08

### Added

- CERN username to terminal prompt
- APT packages
- Welcome message
- Default workspace
- Extension-manager extension to the default workspace
- Information to README concerning this version and a tips/resources section

### Changed

- Configuration files moved to new `binder` directory

## [0.1.0] - 2020-04-28

### Added

- Extensions for showing resource usage:
    - nvdashboard: focused on GPU, but also shows CPU, memory and I/O.
    - nbresuse: showing memory.
    - system-monitor: frontend for nbresuse, being displayed in the top bar.
    - topbar-extension: a system-monitor dependency.
- Kerberos using CERN's [krb5.conf](https://linux.web.cern.ch/docs/krb5.conf) file
- EOS soft link in the landing directory
- This CHANGELOG file.

[0.2.0]: https://gitlab.cern.ch/binder/repos/base/-/releases/v0.2.0/
[0.1.0]: https://gitlab.cern.ch/binder/repos/base/-/releases/v0.1.0/
