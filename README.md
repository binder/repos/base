# Binder Base Image @ CERN [![Binder](https://binder.cern.ch/badge_logo.svg)](https://binder.cern.ch/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fafarinha%2Fbinder-base-image/master)

## Table of contents

- [Overview](#overview)
    - [Running the Base Image in your Browser](#running-the-base-image-in-your-browser)
- [Customization](#customization)
    - [Session Only Customization](#session-only-customization)
    - [Permanent Customization: adding Packages and Configs](#permanent-customization:-adding-packages-and-configs)
- [Resources](#resources)
    - [Tips & Tricks](#tips-&-tricks)
    - [Binder Documentation Highlights](#binder-documentation-highlights)
    - [Server/Image Creation](#server/image-creation)
- [Help & Feedback](#help-&-feedback)

# Overview

This project provides the base for a web notebook-like workspace that is designed to be easily **customizable** and functionality-rich. In the same browser tab you can have, for example, multiple python notebooks and bash terminals open, alongside your favourite JupyterLab extension features (from [vim keybindings](https://github.com/jwkvam/jupyterlab-vim) to [drawio vector graphics](https://github.com/QuantStack/jupyterlab-drawio), and much more: ["99 ways to extend the Jupyter ecosystem"](https://blog.jupyter.org/99-ways-to-extend-the-jupyter-ecosystem-11e5dab7c54)).

The interface is made available by JupyterLab and its appearance can be configured using drag and drop. Here's an example of how it looks with a Python notebook, a terminal (not in focus) and resource monitoring tabs and with the side bar expanded:

![JupyterLab Workspace Example](images/jlab-workspace-eg.png "JupyterLab Workspace Example")

 Currently the **main features** are:
- A working GPU environment with CUDA
- EOS integration (after `kinit <username>`)
- JupyterLab extensions showing resource usage for: GPU, CPU, memory and I/O
- Jupyter Python notebooks
- Linux terminals
- Install extensions interactively (explained [below](#install-extensions-with-the-extension-Manager))

## Running the Base Image in your Browser

For non-GPU workloads, simply press this [![Binder](https://binder.cern.ch/badge_logo.svg)](https://binder.cern.ch/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fafarinha%2Fbinder-base-image/master) icon to launch a server that will run on [CERN's JupyterHub](hub.cern.ch) using the latest version of this repository. After loading is finished, you should be presented with a welcome message with a brief description of the image being used.

If you want control over the hardware on which your image will run, go to [hub.cern.ch](hub.cern.ch), press "Start My Server" and choose the desired server profile. You will also have to input the custom image URL, which for version 0.2.0 is: gitlab-registry.cern.ch/binder/images/build-https-3a-2f-2fgitlab-2ecern-2ech-2fbinder-2frepos-2fbase-0fa484:737575c53fb6921d3f884dd9f547aaa37b5859c7.

# Customization

## Session-only Customization

The customizations mentioned in this section will only last for your current notebook server session.

### **Installing Conda and Pip Packages**

We recommend installing packages with conda rather than pip. When using both package managers conflicts may occur (see [this](https://www.anaconda.com/blog/using-pip-in-a-conda-environment) blog post) so if you have to use both, first install using conda and only then with pip. To install, run the usual install commands in a notebook cell or in a terminal:

```
conda install <package>
```

and with pip:

```
pip install <package>
```


### **Install Extensions with the Extension-Manager**

This is a simple point-and-click way of trying out extensions. Upon opening a server instance you should see a puzzle piece on the left panel, if you click it you can search/discover extensions to install. The image below summarizes this:

<img src="images/extension-manager-explained.png" height="550" alt="Extension Manager Explained">

After the installation you should see a pop-up on the top-left corner asking to rebuild:

<img src="images/extension-manager-rebuild.png" height="200" alt="Extension Manager Rebuild">

Finally, a pop-up asking to reload the page so that you can experiment with your freshly installed extension should come up:

<img src="images/extension-manager-reload.png" height="180" alt="Extension Manager Reload">

Note that these extensions may have dependencies, for example, on the JupyterLab version or on python packages, hence they might not be compatible at first. In this case, the incompatibility should be pointed out to the user by JupyterLab as well as how to solve it. Regardless, if you encounter problems with an extension that you are interested in using, please [drop us a note](#help-&-feedback).

If you liked any of the extension(s) you tried you can also add them permanently, read [below](#permanent-customization:-adding-packages-and-configs).


## Permanent Customization: adding Packages and Configs

The high-level workflow for permanent modification and use of the base image is to:

1. Fork this repository
2. Make changes to the files in the `binder` directory
3. Push your changes
4. Build the image at binder.cern.ch (automatic redirection to a running instance after building)
5. Run it again when you please or with different hardware at hub.cern.ch

### **Important Files**

In the `binder` directory you can find:

- [`apt.txt`](https://gitlab.cern.ch/afarinha/binder-base-image/-/blob/master/apt.txt) &ndash; controls APT package installation
- [`environment.yml`](https://gitlab.cern.ch/afarinha/binder-base-image/-/blob/master/environment.yml) &ndash; controls Conda/pip package installation
- [`postBuild`](https://gitlab.cern.ch/afarinha/binder-base-image/-/blob/master/postBuild) &ndash; contains commands to run after the environment installation
- [`workspace.json`](https://gitlab.cern.ch/afarinha/binder-base-image/-/blob/master/workspace.json) &ndash; defines the default look of the JupyterLab interface

The changes in the above files are typically straightforward: simply add/modify/remove packages or commands similarly to what is already there. The Binder [Configuration Files](https://mybinder.readthedocs.io/en/latest/config_files.html) page gives a complete overview of the details and other available customization options (and we can always try to [help you](#help-&-feedback)).

The `workspace.json` file deserves extra attention. As it is a `json` file, you can visually inspect it and manually apply changes for simple things like changing the default space occupied by an opened terminal tab or the default active tab. However, for more complex changes, for example, if you would like to keep the current look of your session you can store it by running in a terminal:

```
jupyter lab workspaces export > workspace.json
```

and then replacing the `binder/workspace.json` file in your forked repository and pushing these changes. That way, every time you launch your image, you will get presented with the workspace you saved, thus improving your workflow.

> NOTE: the terminal tab you used to export the current workspace gets included in it. In case you don't want it, you can remove it by manually editing the file in the parts where the corresponding `terminal` word appears. Take care not to delete terminal tabs that you do want to keep.

You can take a look at the [binder workspace demo](https://github.com/ian-r-rose/binder-workspace-demo) for more details and adding user settings such as enabling dark mode.


### **Creating and Running a Custom Image**

To build a Binder image you will need a git repository, hence you can start by forking this one. After applying the desired changes in your fork, you can then build the custom image at [binder.cern.ch](binder.cern.ch) using your fork's URL. When building completes, a new server instance with 1 CPU core and 2GB memory is launched.

To adjust the server's hardware (for example, adding a GPU), get the image tag from the [container registry](https://gitlab.cern.ch/binder/images/container_registry) and launch the server from the [hub](hub.cern.ch) (as explained [previously](#running-the-base-image-in-your-browser)).

> NOTE: it is also possible to build the image elsewhere, for example, at mybinder.org, which provides a default hardware profile that does not include GPU resources. There, after the image is built you need to replace the last part of the URL with `/lab` (instead of `/tree`) to get the designed JupyterLab experience. Mybinder supports JupyterLab 2, which may better suit your workflow. We will also provide this version when all our dependencies allow it.

# Resources

### Tips & Tricks

- [Work with notebooks in VSCode while they are running in CERN's Hub](https://gitlab.cern.ch/afarinha/vscode-jupyterhub-integration/-/blob/master/index.md)
- [Example of launching binder with a custom JupyterLab layout](https://github.com/ian-r-rose/binder-workspace-demo)

### Binder Documentation Highlights

- [Homepage](https://mybinder.readthedocs.io/en/latest/)
- [Customization Configuration Files](https://mybinder.readthedocs.io/en/latest/config_files.html)
- [Sharing workspaces](https://mybinder.readthedocs.io/en/latest/howto/lab_workspaces.html?highlight=workspaces)

### Server/Image Creation:

- [CERN Hub &ndash; launch/manage your notebook servers](https://hub.cern.ch/)
- [CERN Binder &ndash; build your JupyterLab image](https://binder.cern.ch/)
- [Public Binder &ndash; build your JupyterLab image](https://mybinder.org/)
- [List previously built images](https://gitlab.cern.ch/binder/images/container_registry)

# Help & Feedback

Do you need help? Have you got ideas or suggestions? Any questions or feedback are always welcome. You can mail me at antonio.paulo@cern.ch or message me directly in Mattermost (@afarinha). Also, feel free to open a merge request with changes you find useful for most of the community.
