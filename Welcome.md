<img src="images/cern-logo.png" width="200px" align="right">

## CERN Binder Base Image

Welcome to the binder base image at CERN, here you can:
- Work with **GPUs** (with an adequate hardware profile selected)
- Access **EOS** directories
- Control **resource usage** of GPU, CPU, memory and I/O
- Launch Python **notebooks**
- Launch **terminals**
- **Install extensions** interactively for the current session

Missing something? This image focuses on being easily **customizable**. For details, please check the [README](https://gitlab.cern.ch/binder/repos/base/-/blob/master/README.md) or [contact us](mailto:antonio.paulo@cern.ch).

**NOTES:**
- Your home path is `/home/jovyan`
- To **access EOS**, please request a Kerberos token with `kinit <cern-user>`
